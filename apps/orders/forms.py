from django import forms
from django.utils.translation import gettext_lazy as _
from apps.customers.models import Customer
from .models import Order, OrderDetail


class OrderForm(forms.ModelForm):
    """
    Form for Order.
    """
    customer = forms.ModelChoiceField(
        queryset=Customer.objects.all(),
        label=u"Customer",
        required=False,
        widget=forms.Select(
            attrs={'class': 'form-control', }
        ),
    )
    class Meta:
        model = Order
        fields = '__all__'
        widgets = {
            'title': forms.TextInput(attrs={'placeholder': 'Ex: First order', 'class': 'form-control', }),
        }
        exclude = ['value', 'discount', 'final_value', ]


class OtherForm(forms.ModelForm):
    """
    Form for Order.
    """
    customer = forms.ModelChoiceField(
        queryset=Customer.objects.all(),
        label=u"Customer",
        required=False,
        widget=forms.Select(
            attrs={'class': 'form-control', }
        ),
    )
    class Meta:
        model = Order
        fields = '__all__'
        exclude = ['value', 'discount', 'final_value', 'title', 'is_paid', ]


class OrderDetailForm(forms.ModelForm):
    """
    Form for Order Detail.
    """
    class Meta:
        model = OrderDetail
        fields = '__all__'
        exclude = ['order', 'discount_price', 'final_price', 'total_price',  ]

