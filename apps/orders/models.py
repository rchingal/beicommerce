import datetime
from decimal import Decimal

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import timezone
from django.db.models import Sum
from django.urls import reverse

from apps.commons.models import AuditBaseAbstract
from apps.products.models import Product
from apps.customers.models import Customer


class Order(AuditBaseAbstract):
    """Model for order fields"""

    title = models.CharField(max_length=150, verbose_name=_('title'),
        help_text=_('Descriptive order title'),)
    value = models.DecimalField(default=0.00, decimal_places=2, max_digits=20)
    discount = models.DecimalField(default=0.00, decimal_places=2, max_digits=20)
    final_value = models.DecimalField(default=0.00, decimal_places=2, max_digits=20)
    is_paid = models.BooleanField(default=True)
    customer = models.ForeignKey(Customer,
        on_delete=models.CASCADE,
        related_name='customers'
    )
    class Meta:
        """Meta data respresentation"""
        db_table = "order"
        ordering = ['-created_at']

    def save(self, *args, **kwargs):
        order_items = self.order_items.all()
        self.value = order_items.aggregate(Sum('total_price'))['total_price__sum'] if order_items.exists() else 0.00
        self.final_value = Decimal(self.value) - Decimal(self.discount)
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title if self.title else 'New Order'

    def get_edit_url(self):
        return reverse('update_order', kwargs={'pk': self.id})

    def get_delete_url(self):
        return reverse('delete_order', kwargs={'pk': self.id})

    @property
    def get_products(self):
        products = self.order_items\
            .select_related(
                "product",
            )
        return products


class OrderDetail(models.Model):
    """Model for order detail fields"""

    product = models.ForeignKey(Product,
        on_delete=models.PROTECT,
        related_name='product_items'
    )
    order = models.ForeignKey(Order,
        on_delete=models.CASCADE,
        related_name='order_items'
    )
    quantity = models.PositiveIntegerField(default=1)
    price = models.DecimalField(default=0.00, decimal_places=2, max_digits=20)
    discount_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=20)
    final_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=20)
    total_price = models.DecimalField(default=0.00, decimal_places=2, max_digits=20)

    class Meta:
        """Meta data respresentation"""
        db_table = "orderdetail"

    def save(self,  *args, **kwargs):
        self.final_price = self.discount_price if self.discount_price > 0 else self.price
        self.total_price = Decimal(self.quantity) * Decimal(self.final_price)
        super().save(*args, **kwargs)
        self.order.save()

    def __str__(self):
        return f'{self.order.title} - {self.product.name}'