from rest_framework import serializers
from rest_framework.response import Response
from apps.products.models import Product
from apps.products.api.serializers import ProductSerializer
from apps.customers.models import Customer
from apps.customers.api.serializers import CustomerSerializer

from ..models import Order, OrderDetail

class OrderSerializer(serializers.ModelSerializer):
    created_at = serializers.SerializerMethodField()
    products = serializers.SerializerMethodField(read_only=True)
    value = serializers.SerializerMethodField(read_only=True)
    final_value = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Order
        fields = [
            'id',
            'title',
            'value',
            'final_value',
            'created_at',
            'customer',
            'products',
        ]

    def get_created_at(self, obj):
        return obj.created_at.date()

    def get_value(self, obj):
        return obj.value

    def get_final_value(self, obj):
        return obj.final_value

    def get_products(self, obj):
        res = ""
        for p in obj.get_products:
            res = res + p.product.name+","
        return res


class OrderDetailSerializer(serializers.ModelSerializer):
    product = serializers.SlugRelatedField(queryset=Product.objects.all(), slug_field='name')
    total_price = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = OrderDetail
        fields = [
            'id',
            'order',
            'quantity',
            'price',
            'total_price',
            "product",
        ]

    def get_total_price(self, obj):
        return obj.total_price