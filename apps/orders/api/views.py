from django.utils.translation import gettext_lazy as _
from rest_framework import status, generics
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication

from apps.customers.models import Customer
from .serializers import OrderSerializer, OrderDetailSerializer
from .pagination import OrderPageNumberPagination
from ..models import Order, OrderDetail
from ..forms import OrderDetailForm

class OrderList(generics.ListCreateAPIView):
    """
    Order List View
    """
    serializer_class = OrderSerializer
    queryset = Order.objects.all()
    name = 'Order List'
    permission_classes = [IsAuthenticated,]
    authentication_class = (TokenAuthentication,)
    pagination_class = OrderPageNumberPagination

    def get_queryset(self):
        kwargs = {}
        start_date = self.request.GET.get('start_date')
        end_date = self.request.GET.get('end_date')
        customer = self.request.GET.get('customer')
        if customer:
            kwargs['customer_id'] = customer
        if start_date and end_date:
            kwargs['created_at__range'] = (start_date, end_date)
        if start_date:
            kwargs['created_at__gte'] = start_date
        if end_date:
            kwargs['created_at__lte'] = end_date

        return Order.objects.filter(**kwargs)

    def create(self, request, *args, **kwargs):
        customer_id = request.data.get("customer")
        customer = Customer.objects.filter(id=customer_id).first()
        if not customer:
            res = {
                "error": 'The customer {} does not exist.'.format(customer_name),
            }
            return Response(res, status=status.HTTP_401_UNAUTHORIZED)

        if customer.products.count() <= 5:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            res = {
                "error": 'The number of available products must be less than 5',
            }
            return Response(res, status=status.HTTP_401_UNAUTHORIZED)



class OrderUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    """
    Order, Update and Destroy View
    """
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    name = 'Order Update and destroy'
    lookup_field = 'pk'
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]


class OrderDetailView(generics.ListCreateAPIView):

    name = 'List and Create Order Detail'
    serializer_class = OrderDetailSerializer
    queryset = OrderDetail.objects.all()

    #authentication_classes = (TokenAuthentication,)
    #permission_classes = [IsAuthenticated, ]

    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)
        return Response({
            'status': status.HTTP_200_OK,
            'message': 'Order detail save successful!',
            'data': response.data
        })