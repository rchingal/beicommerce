from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination

class OrderLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 5
    max_limit = 1000

class OrderPageNumberPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 1000