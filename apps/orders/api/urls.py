from django.urls import path, include

from .views import OrderList, OrderUpdateDestroyView, OrderDetailView

urlpatterns = [
    # Orders
    path('orders/', OrderList.as_view(), name=OrderList.name),
    path('orders/<int:pk>/', OrderUpdateDestroyView.as_view(), name=OrderUpdateDestroyView.name),
    path('orders/<int:pk>/detail/', OrderDetailView.as_view(), name=OrderDetailView.name),
]
