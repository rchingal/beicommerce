# Generated by Django 3.1.7 on 2021-04-07 02:05

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0003_auto_20210404_1945'),
        ('orders', '0004_auto_20210404_2005'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderdetail',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='product_items', to='products.product'),
        ),
    ]
