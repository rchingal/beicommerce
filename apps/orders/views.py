from django.db.models import Q
from django.db import transaction
from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.views.generic import DeleteView, TemplateView

from apps.commons.views import ListViewMixin, CreateAndUpdateViewMixin
from apps.commons.helpers import uw_add_alert

from apps.customers.models import Customer

from .models import Order, OrderDetail
from .forms import OrderForm, OtherForm, OrderDetailForm

@method_decorator(login_required, name='dispatch')
class OtherListView(TemplateView):
    """
    List view for other orders
    """
    name = 'other_list'
    template_name='orders/other_list.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        other_form = OtherForm()
        context["customers"] = Customer.objects.all()
        context["form"] = other_form
        return context


@method_decorator(login_required, name='dispatch')
class OrderListView(ListViewMixin):
    """
    List view for orders
    """
    name = "order_list"
    model = Order
    paginate_by = 8

    def get_queryset(self, tab=None):
        """
        Queryset giving only the current forms
        """
        keyword = self.request.GET.get('keyword', None)
        queryset = self.model.objects.all().order_by('-id')
        if keyword:
            queryset = self.model.objects.filter(
                Q(title__icontains=keyword) |
                Q(customer__name__icontains=keyword)
            )
        return queryset

    def get_success_url(self):
        return reverse_lazy("orders:order_list")



@method_decorator(login_required, name='dispatch')
class CreateAndUpdateOrderView(CreateAndUpdateViewMixin):
    """
    Create and update order
    """
    model = Order
    form_class = OrderForm

    @transaction.atomic
    def form_valid(self, form, **kwargs):
        """
        Validate and save the form with create or update option
        """
        sid = transaction.savepoint()
        try:
            obj = form.save(commit=False)
            if obj.customer.products.count() <= 5:
                obj.save()
                form.save_m2m()
                msg = _('Order save successful!')
                uw_add_alert(self.request, msg,type_message="success")
            else:
                msg = _('The number of available products must be less than 5')
                uw_add_alert(self.request, msg,type_message="error")
            transaction.savepoint_commit(sid)
            return redirect(self.get_success_url())
        except Exception as e:
            uw_add_alert(self.request, e, "error")
            transaction.savepoint_rollback(sid)
        return super().form_invalid(form)

    def get_success_url(self):
        return reverse_lazy("orders:order_list")


@method_decorator(login_required, name='dispatch')
class OrderDeleteView(DeleteView):
    """
    Order delete view
    """
    model = Order

    def get_success_url(self):
        return reverse_lazy("orders:order_list")


@method_decorator(login_required, name='dispatch')
class CreateOrderDetailView(CreateAndUpdateViewMixin):
    """
    Create order detail
    """
    model = OrderDetail
    form_class = OrderDetailForm
    order = None

    def dispatch(self, *args, **kwargs):
        order_id = self.kwargs.get("order_id", None)
        self.order = Order.objects.filter(id=order_id).first()
        return super().dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        """Return an instance of the form to be used in this view."""
        form = super().get_form(form_class)
        products = self.order.customer.products.all()
        form.fields['product'].queryset = products
        return form

    @transaction.atomic
    def form_valid(self, form, **kwargs):
        """
        Validate and save the form with create or update option
        """
        sid = transaction.savepoint()
        try:
            obj = form.save(commit=False)
            obj.order = self.order
            obj.save()
            msg = _('Order detail save successful!')
            uw_add_alert(self.request, msg,type_message="success")
            transaction.savepoint_commit(sid)
            return redirect(self.get_success_url())
        except Exception as e:
            uw_add_alert(self.request, e, "error")
            transaction.savepoint_rollback(sid)
        return super().form_invalid(form)

    def form_invalid(self, form):
        print(form)
        return super().form_invalid(form)

    def get_success_url(self):
        return reverse_lazy("orders:order_list")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["order"] = self.order
        return context