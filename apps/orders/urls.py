from django.urls import path

from .views import *

app_name = "orders"

urlpatterns = [
    # Order
    path('orders/', OrderListView.as_view(), name=OrderListView.name),
    path('others/', OtherListView.as_view(), name=OtherListView.name),
    path('orders/create/', CreateAndUpdateOrderView.as_view(), name='order_create'),
    path('orders/<int:pk>/update/', CreateAndUpdateOrderView.as_view(), name='order_update'),
	path('orders/<int:pk>/delete/', OrderDeleteView.as_view(), name='order_delete'),

    # Order Detail
    path('orders/<int:order_id>/detail/', CreateOrderDetailView.as_view(), name='order_detail_create'),
]