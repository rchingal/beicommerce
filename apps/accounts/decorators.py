from django.shortcuts import redirect

def is_superuser(function):
    """decorator for redirect to admin:index"""
    def _function(request,*args, **kwargs):
        if request.user.is_authenticated and request.user.is_superuser:
            return redirect('admin:index')
        return function(request, *args, **kwargs)
    return _function