from http import HTTPStatus
from django.test import TestCase, Client
from django.urls import reverse
from django.core import mail
from django.contrib.sites.models import Site
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken


from .forms import CustomPasswordResetForm, SignUpForm, CustomAuthenticationForm
from .models import User


class LoginPostRequest(TestCase):
    """This test creates a post request and sends user data,
    simulating that the entire authentication system is activated,
    if the system works, the http it returns is ok, testing all the login"""

    def test_title_starting_lowercase(self):
        response = self.client.post(
            "/accounts/login/",
            data={
                "email": "sfds@hotmail.com",
                "password": "pafdatrik"}
        )
        self.assertEqual(response.status_code, HTTPStatus.OK)


class JwtWork(TestCase):

    def test_one(self):
        user = User.objects.create_user(email='usuario@mail.com',
                                        password='contrasegna')
        client = APIClient()
        refresh = RefreshToken.for_user(user)
        client.credentials(HTTP_AUTHORIZATION=f'Bearer {refresh.access_token}')

        return client

    def executing_token(self, test_one):
        url = reverse("api_login")
        response = test_one.get(url)
        data = response.data
        self.assertEqual(status.HTTP_200_OK)


class SetUpClass(TestCase):

    def setUp(self):
        self.user = User(email = "dev29@aiatic.com", password = "dev29aiatic")
        self.user.save()
        self.c = Client()


class PassswordRecoveryTest(SetUpClass):
    def test_passwordresetform(self):
        data = {'email' : 'example@hotmail.com'}
        form = CustomPasswordResetForm(data=data)
        self.assertTrue(form.is_valid())

    def test_sendresetform(self):
        response = self.c.post(reverse('accounts:password_reset'), {'email': 'dev29@aiatic.com'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(mail.outbox), 1)
        token = response.context[0]['token']
        uid = response.context[0]['uid']
        response = self.c.post(reverse('accounts:password_reset_confirm', kwargs={'token': token, 'uidb64' : uid}),
                                {'new_password1': 'ContraseñaEjemplo123', 'new_password2': 'ContraseñaEjemplo123'})
        self.assertEqual(response.status_code, 302)


class SetupClass(TestCase):

    def setUp(self):
        self.kwars = {}
        self.kwars['email'] = 'test@test.com'
        self.kwars['password'] = 'superpass'
        self.user = User.objects.create_user(**self.kwars)


class LoginFormTest(SetupClass):
    """
    Login form test on `accounts.User`.
    """
    def test_LoginForm_valid(self):
        """
        Valid Login Form Data
        """
        data = {'username': "test@test.com", 'password': "superpass"}
        form = CustomAuthenticationForm(data=data)
        self.assertTrue(form.is_valid())

    def test_LoginForm_invalid(self):
        """
        Invalid Login Form Data
        """
        data = {'username': "email@email.com", 'password': "otherpass"}
        form = CustomAuthenticationForm(data=data)
        self.assertFalse(form.is_valid())


class SignUpFormTest(SetupClass):
    """
    SignUp form test on `accounts.User`.
    """
    def test_SignUpForm_valid(self):
        """
        Valid SignUp Form Data
        """
        data = {'email': "test1@email.com", 'password1': "superpass123", 'password2': "superpass123",}
        form = SignUpForm(data=data)
        self.assertTrue(form.is_valid())

    def test_SignUpForm_invalid(self):
        """
        # Invalid SignUp Form Data
        """
        data = {'email': "test2@email.com", 'password1': "superuser", 'password2': "superuser1"}
        form = SignUpForm(data=data)
        self.assertFalse(form.is_valid())


class LoginViewsTest(SetupClass):
    """
    Login View test on `accounts.User`.
    """
    def test_login_template(self):
        """
        Validate Login url
        """
        res = self.client.get(reverse('accounts:login'))
        self.assertEqual(res.status_code, 200)

    def test_home_view(self):
        """
        Validate Home view
        """
        user_login = self.client.login(email=self.kwars['email'], password=self.kwars['password'])
        self.assertTrue(user_login)
        res = self.client.get(reverse('home'))
        self.assertEqual(res.status_code, 302)

    def test_login_view(self):
        """
        Validate Login view
        """
        data = {'username': self.kwars['email'], 'password': self.kwars['password']}
        res = self.client.post('/accounts/login', data)
        self.assertEqual(res.status_code, 301)


class SignUpViewsTest(SetupClass):
    """
    SignUp View test on `accounts.User`.
    """
    def test_signup_template(self):
        """
        Validate SignUp url
        """
        res = self.client.get(reverse('accounts:signup'))
        self.assertEqual(res.status_code, 200)

    def test_signup_view(self):
        """
        Validate SignUp view
        """
        data = {'username': self.kwars['email'], 'password': self.kwars['password']}
        res = self.client.post('/signup', data)
        self.assertEqual(res.status_code, 301)