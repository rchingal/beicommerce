from django.db import models
from django.utils.translation import gettext_lazy as _


class AuditBaseAbstract(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_('created date'),
        help_text=_('created date'), blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_('updated date'),
        help_text=_('updated date'), blank=True, null=True)

    class Meta:
        abstract = True