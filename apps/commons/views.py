from rest_framework.generics import get_object_or_404
from django.views.generic import ListView, UpdateView
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required


@method_decorator(login_required, name='dispatch')
class ListViewMixin(ListView):
    """
    Data List view for all applications
    """

    def get_queryset(self, tab=None):
        """
        Queryset giving only the current forms
        """
        keyword = self.request.GET.get('keyword', None)
        queryset = self.model.objects.all().order_by('-id')
        if keyword:
            queryset = self.model.objects.filter(
                name__icontains=keyword
            )
        return queryset


@method_decorator(login_required, name='dispatch')
class CreateAndUpdateViewMixin(UpdateView):
    """
    Data view (create and update)
    """
    object = None
    object_id = None

    def dispatch(self, *args, **kwargs):
        self.object_id = self.kwargs.get("pk", None)
        return super().dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        """
        Return: request quote object by pk
        """
        if self.object_id:
            self.object = get_object_or_404(self.model, pk=self.object_id)
            return self.object
        return None

