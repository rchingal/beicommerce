from django.urls import path, include

from .views import CustomerList, CustomerUpdateDestroyView

urlpatterns = [
    # Customers
    path('customers/', CustomerList.as_view(), name=CustomerList.name),
    path('customers/<int:pk>/', CustomerUpdateDestroyView.as_view(), name=CustomerUpdateDestroyView.name),
]