from django.utils.translation import gettext_lazy as _
from rest_framework import status, generics
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication


from .serializers import CustomerSerializer
#from .permissions import IsOwnerOrReadOnly
from .pagination import CustomerPageNumberPagination

from ..models import Customer


class CustomerList(generics.ListCreateAPIView):
    """
    Customer List View
    """
    serializer_class = CustomerSerializer
    queryset = Customer.objects.all()
    name = 'Customer List'
    permission_classes = [IsAuthenticated,]
    authentication_class = (TokenAuthentication,)
    pagination_class = CustomerPageNumberPagination

    def get_queryset(self):
        name = self.request.GET.get('name')
        email = self.request.GET.get('email')
        kwargs = {}
        if name:
            kwargs['name'] = name
        if email:
            kwargs['email'] = email

        return Customer.objects.filter(**kwargs)

    def perform_create(self, serializer):
        serializer.save()


class CustomerUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Customer.objects.all()
    serializer_class = CustomerSerializer
    name = 'Customer Update and destroy'
    lookup_field = 'pk'
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]