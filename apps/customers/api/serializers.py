from rest_framework import serializers

from apps.products.api.serializers import ProductSerializer
from apps.products.models import Product
from ..models import Customer

class CustomerSerializer(serializers.ModelSerializer):
    products = serializers.SlugRelatedField(queryset=Product.objects.all(), slug_field='name')
    class Meta:
        model = Customer
        fields = [
            'id',
            'name',
            'email',
            'products',
        ]