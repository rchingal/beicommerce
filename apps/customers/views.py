from django.db import transaction
from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.views.generic import DeleteView

from apps.commons.views import ListViewMixin, CreateAndUpdateViewMixin
from apps.commons.helpers import uw_add_alert

from .models import Customer
from .forms import CustomerForm


@method_decorator(login_required, name='dispatch')
class CustomerListView(ListViewMixin):
    """
    List view for fields
    """
    name = "customer_list"
    model = Customer
    paginate_by = 8

    def get_success_url(self):
        return reverse_lazy("customers:customer_list")



@method_decorator(login_required, name='dispatch')
class CreateAndUpdateCustomerView(CreateAndUpdateViewMixin):
    """
    Create and update customer
    """
    model = Customer
    form_class = CustomerForm

    @transaction.atomic
    def form_valid(self, form, **kwargs):
        """
        Validate and save the form with create or update option
        """
        sid = transaction.savepoint()
        try:
            obj = form.save(commit=False)
            obj.save()
            form.save_m2m()
            msg = _('Customer save successful!')
            uw_add_alert(self.request, msg,type_message="success")
            transaction.savepoint_commit(sid)
            return redirect(self.get_success_url())
        except Exception as e:
            uw_add_alert(self.request, e, "error")
            transaction.savepoint_rollback(sid)
        return super().form_invalid(form)

    def get_success_url(self):
        return reverse_lazy("customers:customer_list")


@method_decorator(login_required, name='dispatch')
class CustomerDeleteView(DeleteView):
    """
    Customer delete view
    """
    model = Customer

    def get_success_url(self):
        return reverse_lazy("customers:customer_list")