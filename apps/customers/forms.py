from django import forms
from django.utils.translation import gettext_lazy as _
from apps.products.models import Product
from .models import Customer


class CustomerForm(forms.ModelForm):
    """
    Form for Customer.
    """
    products = forms.ModelMultipleChoiceField(
        queryset=Product.objects.all(),
        label=u"Available Products",
        required=False,
        widget=forms.CheckboxSelectMultiple(),
    )
    class Meta:
        model = Customer
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Ex: Jhon Doe', 'class': 'form-control', }),
            'email': forms.EmailInput(attrs={'placeholder': 'Ex: jhon.doe@gmail.com', 'class': 'form-control', }),
        }
        exclude = ['final_value', 'discount_value', ]