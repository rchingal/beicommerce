from django.urls import path

from .views import *

app_name = "customers"

urlpatterns = [
    path('customers/', CustomerListView.as_view(), name=CustomerListView.name),
    path('customers/create/', CreateAndUpdateCustomerView.as_view(), name='customer_create'),
    path('customers/<int:pk>/update/', CreateAndUpdateCustomerView.as_view(), name='customer_update'),
	path('customers/<int:pk>/delete/', CustomerDeleteView.as_view(), name='customer_delete'),
]