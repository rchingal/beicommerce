from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.accounts.models import PersonAbstract
from apps.products.models import Product


class Customer(PersonAbstract):
    """Model for customer fields"""

    name = models.CharField(max_length=191, verbose_name=_('name'),
        help_text=_('customer name'),)
    email = models.EmailField(unique=True, error_messages={'unique': _("A customer with this email already exists."), },
        verbose_name=_('Email address'),
        help_text=_('The email address is the same username.'))

    products = models.ManyToManyField(Product, blank=True,
        verbose_name=_('available products'),
        help_text=_('available products by customer')
    )

    class Meta:
        """Meta data respresentation"""
        db_table = "customer"

    def __str__(self):
        return self.name
