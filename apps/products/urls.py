from django.urls import path

from .views import *

app_name = "products"

urlpatterns = [
    path('products/', ProductListView.as_view(), name=ProductListView.name),
    path('products/create/', CreateAndUpdateProductView.as_view(), name='product_create'),
    path('products/<int:pk>/update/', CreateAndUpdateProductView.as_view(), name='product_update'),
	path('products/<int:pk>/delete/', ProductDeleteView.as_view(), name='product_delete'),
]