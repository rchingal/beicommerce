# Generated by Django 3.1.7 on 2021-04-05 00:45

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0002_auto_20210404_1434'),
    ]

    operations = [
        migrations.AlterModelTable(
            name='product',
            table='product',
        ),
    ]
