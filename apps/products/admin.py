from django.contrib import admin

from .models import Product


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'final_value', 'active']
    list_filter = ['active', ]
    search_fields = ['name']
    list_per_page = 50
    fields = ['active', 'name', 'price', 'description', 'discount_value',]