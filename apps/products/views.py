from django.db import transaction
from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.utils.decorators import method_decorator
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.decorators import login_required
from django.views.generic import DeleteView

from apps.commons.views import ListViewMixin, CreateAndUpdateViewMixin
from apps.commons.helpers import uw_add_alert

from .models import Product
from .forms import ProductForm


@method_decorator(login_required, name='dispatch')
class ProductListView(ListViewMixin):
    """
    List view for fields
    """
    name = "product_list"
    model = Product
    paginate_by = 8

    def get_success_url(self):
        return reverse_lazy("products:product_list")



@method_decorator(login_required, name='dispatch')
class CreateAndUpdateProductView(CreateAndUpdateViewMixin):
    """
    Create or update a product
    """
    model = Product
    form_class = ProductForm

    @transaction.atomic
    def form_valid(self, form, **kwargs):
        """
        Validate and save the form with create or update option
        """
        sid = transaction.savepoint()
        try:
            obj = form.save(commit=False)
            obj.save()
            msg = _('Product save successful!')
            uw_add_alert(self.request, msg,type_message="success")
            transaction.savepoint_commit(sid)
            return redirect(self.get_success_url())
        except Exception as e:
            uw_add_alert(self.request, e, "error")
            transaction.savepoint_rollback(sid)
        return super().form_invalid(form)

    def get_success_url(self):
        return reverse_lazy("products:product_list")


@method_decorator(login_required, name='dispatch')
class ProductDeleteView(DeleteView):
    """
    Product delete view
    """
    model = Product

    def get_success_url(self):
        return reverse_lazy("products:product_list")