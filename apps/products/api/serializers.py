from rest_framework import serializers

from ..models import Product

class ProductSerializer(serializers.ModelSerializer):
    description = serializers.SerializerMethodField()

    class Meta:
        model = Product
        fields = [
            'id',
            'name',
            'description',
            'active',
            'price',
        ]

    def get_description(self, obj):
        if obj.description:
            description = obj.description
        else:
            description = "--"
        return description