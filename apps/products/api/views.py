from django.utils.translation import gettext_lazy as _
from rest_framework import status, generics
from rest_framework.permissions import AllowAny, IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.authentication import TokenAuthentication


from .serializers import ProductSerializer
from .pagination import ProductPageNumberPagination

from ..models import Product


class ProductList(generics.ListCreateAPIView):
    """
    Product List View
    """
    serializer_class = ProductSerializer
    queryset = Product.objects.all()
    name = 'Product List'
    permission_classes = [IsAuthenticated,]
    authentication_class = (TokenAuthentication,)
    pagination_class = ProductPageNumberPagination

    def get_queryset(self):
        name = self.request.GET.get('name')
        active = self.request.GET.get('active')
        kwargs = {}
        if name:
            kwargs['name'] = name
        if active:
            kwargs['active'] = active

        return Product.objects.filter(**kwargs)

    def perform_create(self, serializer):
        serializer.save()


class ProductUpdateDestroyView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    name = 'Product Update and destroy'
    lookup_field = 'pk'
    authentication_classes = (TokenAuthentication,)
    permission_classes = [IsAuthenticated,]