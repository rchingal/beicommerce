from django.urls import path, include

from .views import ProductList, ProductUpdateDestroyView

urlpatterns = [
    # Products
    path('products/', ProductList.as_view(), name=ProductList.name),
    path('products/<int:pk>/', ProductUpdateDestroyView.as_view(), name=ProductUpdateDestroyView.name),
]
