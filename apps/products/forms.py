from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Product

class ProductForm(forms.ModelForm):
    """
    Form for Product.
    """

    class Meta:
        model = Product
        fields = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'placeholder': 'Ex: Product test','class': 'form-control', }),
            'description': forms.TextInput(
                attrs={'placeholder': 'Ex: This is a product test', 'autofocus': True, 'class': 'form-control search-round'}
            ),
            'price': forms.NumberInput(attrs={'class': 'form-control', }),
        }
        exclude = ['final_value', 'discount_value', ]