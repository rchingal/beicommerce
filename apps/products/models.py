from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.commons.models import AuditBaseAbstract
from .managers import ProductManager


class Product(AuditBaseAbstract):
    """Model for product fields"""

    name = models.CharField(max_length=150, unique=True, verbose_name=_('name'),
        help_text=_('Descriptive product name'),)
    description = models.TextField(verbose_name=_('description'),
        help_text=_('Description product'), blank=True, null=True)
    active = models.BooleanField(default=True)
    price = models.DecimalField(default=0.00, decimal_places=2, max_digits=10)
    discount_value = models.DecimalField(default=0.00, decimal_places=2, max_digits=10)
    final_value = models.DecimalField(default=0.00, decimal_places=2, max_digits=10)
    #quantity = models.PositiveIntegerField(default=0)

    objects = models.Manager()
    broswer = ProductManager()

    class Meta:
        """Meta data respresentation"""
        db_table = "product"
        verbose_name_plural = 'Products'

    def save(self, *args, **kwargs):
        self.final_value = self.discount_value if self.discount_value > 0 else self.price
        super().save(*args, **kwargs)

    def __str__(self):
        return self.name