BEICOMMERCE
==========

BEICOMMERCE backend on Django with Docker

Requirements
------------

* `Docker <https://docs.docker.com/install>`_
  — Install Docker for different operating system. See documentation.
* `Docker Compose <https://docs.docker.com/compose/install/>`_
  — Install Docker Compose for MacOS, Windows, and Linux
* `Gitlab Runner <https://docs.gitlab.com/runner/>`_
  — Install gitlab runner for macOS, Windows, Linux and FreeBSD
* `Python 3.8 <https://www.python.org/>`_
  — Programming language
* `Django <https://www.djangoproject.com/>`_
  — The django requirements packages are in the requirements.txt file
* `Postgres <https://www.postgresql.org/>`_
  — The user credentials are in the environment variable file called: .env
* `Nginx <https://nginx.org/en/>`_
  — Nginx is used as a reverse proxy.
* `RabbitMQ <https://www.rabbitmq.com/>`_
  — RabbitMQ is a message broker by queues.
* `Django Rest Framework <https://www.django-rest-framework.org/>`_
  — Django Rest Framework is a tool for building Web APIs.

LOCAL
================

* **Build or rebuild web project**: ``$ docker-compose build``
* **Start web project as a service in docker**: ``$ docker-compose up``
* **Stops and removes containers, networks, volumes, and images in docker**: ``$ docker-compose down``
* **Create Django project**: ``$ docker-compose run web django-admin.py startproject <project_name> .``
* **Launch the development server**: ``$ docker-compose run web python manage.py runserver 0.0.0.0:8000``
* **Run makemigrations command on Django project**: ``$ docker-compose run web python manage.py makemigrations``
* **Run migrate command on Django project**: ``$ docker-compose run web python manage.py migrate``
* **Create superuser on Django project**: ``$ docker-compose run web python manage.py createsuperuser``
* **Run test command on Django project**: ``$ docker-compose run web python manage.py test``

BEICOMMERCE DEVELOP
===================

Build or rebuild web project with beicommerce-dev.yml:

.. code-block:: bash

    $ docker-compose -f beicommerce-dev.yml build

Start web project as a service in docker.:

.. code-block:: bash

    $ docker-compose -f beicommerce-dev.yml up

Stops and removes containers, networks, volumes, and images in docker.:

.. code-block:: bash

    $ docker-compose -f beicommerce-dev.yml down


Ednpoint
========

Api Root
========
* **URL**: ``/api/v1/``

* **METHOD**: ``GET``

* **Success Response**:
    * **Code**: ``200 OK``
    * **Content**:

.. code-block:: json

    {
        "users": "http://localhost:8000/api/v1/accounts/users/",
        "products": "http://localhost:8000/api/v1/products/",
        "customers": "http://localhost:8000/api/v1/customers/",
        "orders": "http://localhost:8000/api/v1/orders/"
    }

Login
=================

* **URL**: ``/api/v1/accounts/login/``

* **METHOD**: ``POST``

* **DATA PARAMS**: ``{"username": test@test.com, "password": "********" }``

* **Success Response**:
    * **Code**: ``200 OK``
    * **Content**:

.. code-block:: json

    {
        "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoicmVmcmVzaCIsImV4cCI6MTYxNzg1Njg5NywianRpIjoiYmNmOTVmYzU1ZThlNGI4ZDk5YTU0OWE0OWZhNzZkYTYiLCJ1c2VyX2lkIjoyfQ.xNKc8tHxf6H55ElRh8I_V8RY_2y4T6M75kKCk6yjtis",
        "access": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjE3NzcwNzk3LCJqdGkiOiIyY2IzMTI0ZmY2Y2U0NzA3ODRmZTFmMWVhYjdmOGUwNSIsInVzZXJfaWQiOjJ9.yjVnyhu7EPgdPGvHcO9UzFiKvjTEXjJ4EUkzzkUrN9I"
    }


List Orders
============

* **URL**: ``/api/v1/orders/``

* **METHOD**: ``GET``

* **Header** *(Authorization)* : *Bearer* ``xxxxxxx-12345-aaaa-bbbb-9876-xxxx``

* **URL Params Filter**: ``start_date``, ``end_date``

.. code-block:: json

    ?start_date=2018-12-12
    ?end_date=2018-12-12
    ?start_date=2018-11-12&end_date=2018-12-12

* **Success Response**:
    * **Code**: ``200 OK``
    * **Content**:

.. code-block:: json

       {
            "count": 3,
            "next": null,
            "previous": null,
            "results": [
            {
                "id": 3,
                "title": "Third order",
                "value": "250000.00",
                "final_value": "250000.00",
                "created_at": "2021-04-07",
                "customer": {
                    "id": 3,
                    "name": "Mike Simm",
                    "email": "mike.simm@gmail.com",
                    "products": [
                        {
                            "id": 1,
                            "name": "Silla Gamer Nibio Profesional Gs-06",
                            "description": "--",
                            "active": true,
                            "price": "659900.00"
                        },
                        {
                            "id": 2,
                            "name": "Moto G8 Power Lite 64gb + 4gb",
                            "description": "--",
                            "active": true,
                            "price": "489900.00"
                        },
                        {
                            "id": 8,
                            "name": "Tenis Metallo Gris Para Hombre Croydon",
                            "description": "--",
                            "active": true,
                            "price": "89900.00"
                        },
                        {
                            "id": 10,
                            "name": "Bicicicleta Roadmaster Hurricane 29 Shimano Revoshift 21vel",
                            "description": "--",
                            "active": true,
                            "price": "1399900.00"
                        },
                        {
                            "id": 15,
                            "name": "Kit De Pesas Ejercicio Mancuernas Juego De Pesas Termoforrad",
                            "description": "--",
                            "active": true,
                            "price": "389900.00"
                        }
                    ]
                },
                "products": "Kit De Pesas Ejercicio Mancuernas Juego De Pesas Termoforrad,"
            },
       }

Create Order
=============

* **URL**: ``/api/v1/orders/``

* **METHOD**: ``POST``

* **Header** *(Authorization)* : *Bearer* ``xxxxxxx-12345-aaaa-bbbb-9876-xxxx``

* **Data Params:**:

.. code-block:: json

    {"title": "First order", "customer": 1}


* **Customer**:
    * *ID*: The customer in the DB.

* **Success Response**:
    * **Code**: ``200 OK``
    * **Content**:

.. code-block:: json

    {
        "id": 1,
        "title": "First order",
        "value": 0.0,
        "final_value": 0.0,
        "created_at": "2021-04-07",
        "customer": 1,
        "products": ""
    }

List Order Detail
=================

* **URL**: ``/api/v1/orders/<order_id>/detail/``

* **METHOD**: ``GET``

* **Header** *(Authorization)* : *Bearer* ``xxxxxxx-12345-aaaa-bbbb-9876-xxxx``

* **URL Params**: ``order_id``

.. code-block:: json

    order_id=1

* **Success Response**:
    * **Code**: ``200 OK``
    * **Content**:

.. code-block:: json

       [
            {
                "id": 1,
                "order": 2,
                "quantity": 2,
                "price": "50000.00",
                "total_price": 100000.0,
                "product": "Camiseta Cuello Redondo Blanca Estampada Hombre Mizu"
            },
            {
                "id": 2,
                "order": 1,
                "quantity": 10,
                "price": "10000.00",
                "total_price": 100000.0,
                "product": "Bicicletas De Impulso Aprendizaje Aluminio Bikid Strider"
            }
        ]

Create Order Detail
===================

* **URL**: ``/api/v1/orders/<order_id>/detail/``

* **METHOD**: ``POST``

* **Header** *(Authorization)* : *Bearer* ``xxxxxxx-12345-aaaa-bbbb-9876-xxxx``

* **URL Params**: ``order_id``

.. code-block:: json

    order_id=1

* **Data Params:**:

.. code-block:: json

    {
        "order": 1,
        "quantity": 1,
        "price": 2,
        "product": "Silla Gamer Nibio Profesional Gs-06"
    }


* **Customer**:
    * *ID*: The customer in the DB.

* **Success Response**:
    * **Code**: ``200 OK``
    * **Content**:

.. code-block:: json

    {
        "status": 200,
        "message": "Order detail save successful!",
        "data": {
            "id": 16,
            "order": 1,
            "quantity": 1,
            "price": "2.00",
            "total_price": 2.0,
            "product": "Silla Gamer Nibio Profesional Gs-06"
        }
    }


Credits
-------

``Beicommerce`` was created by Rodrigo Chingal (`@rchingal
<https://gitlab.com/rchingal>`_)